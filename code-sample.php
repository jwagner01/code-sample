<?php

/**
 * Simple PHP Code Sample
 * @author	Jason M. Wagner <jwagner01@gmail.com>
 * @version	1.0
 */

include_once ("imporatantFile.php"); // Include a really important file.

 /* Let's do some MySQL stuff, starting with the connection parameters */
$conn = mysqli_connect("localhost", "userfoo", "passfoo", "dbfoo");

/* Now a nicely formatted query to select all users named 'foo'. */
$query = "SELECT `id`, `name`, `email` FROM `tablefoo` "
       . "WHERE `name` = 'foo' "
       . "ORDER BY `id` ASC";
      
/* Now we display the results a a successful query. */
if ( $result = mysqli_query ($conn, $query) ) {
	while ( $row = mysqli_fetch_array($result) ) {
		print_r ($row);
		echo "<br />";
	}
}
$rowsSelected = mysqli_num_rows ($result); // Number of rows from last query.
echo "</p>Rows Selected: $rowsSelected</p>";

class fooClass {
	public $fooVar;
	public $barVar;
	public $bazVar;

	/**
	 * Setting us up for Foobar
	 */
	public function __construct() {
		$this->fooVar = 'foo';
		$this->barVar = 'bar';
		$this->bazVar = 'baz';
	}
	
	/**
	 * Let's Make Some Foobar
	 */
	public function getFoobar() {
		$foobar = $this->fooVar . $this->barVar . " ";
		return str_repeat($foobar, 42);
	}
}

$fooObj = new fooClass();
echo "<p>" . $fooObj->getFoobar() . "</p>";

/* Now there's a handsome array... */
$fooArr = array(
	'iFoo'	=>	$fooObj->fooVar,	// just a few
	'iBar'	=>	$fooObj->barVar,	// nice inline
	'iBaz'	=>	$fooObj->bazVar,	// comments here
);

print_r ($fooArr);

/* Let's make a superfluous function that has some fun with superglobals. */
function getPath() {
	/* Determine and return the full document path */ 
	return $_SERVER['DOCUMENT_ROOT'] . $_SERVER['PHP_SELF'];
}

echo "path=" . getPath();

phpinfo(); // display server config

?>
